#!/bin/sh

set -e

DEB_UPSTREAM_VERSION=$1
DEB_SOURCE_VERSION=$2
NDPI_REV=$3

UPSTREAM_DIR=$(echo -n $DEB_UPSTREAM_VERSION | sed -e 's/+rc[0-9]*//')

curdir=$(dirname $0)

if [ ! -f ../ntop_$DEB_SOURCE_VERSION.orig.tar.gz ]; then
    uscan --noconf --force-download --rename --download-current-version --destdir=.
    rm -rf ntop-$DEB_UPSTREAM_VERSION
    rm -rf ntop-$UPSTREAM_DIR
    rm -rf ntop-$DEB_SOURCE_VERSION
    tar zxf ntop_$DEB_UPSTREAM_VERSION.orig.tar.gz
    rm ntop_$DEB_UPSTREAM_VERSION.orig.tar.gz
    # remove files not DFSG-compliant
    rm ntop-$UPSTREAM_DIR/ntop_darwin.*
    rm ntop-$UPSTREAM_DIR/ntop_win32.*
    rm ntop-$UPSTREAM_DIR/packages/Win32/ntop-VC6-project.zip

    # remove binary-onyl files from MaxMind
    rm ntop-$UPSTREAM_DIR/3rd_party/Geo*.dat.gz

    # replace non-DFSG countmin with the GPL one
    rm ntop-$UPSTREAM_DIR/countmin.[ch]
    rm ntop-$UPSTREAM_DIR/prng.[ch]

    cp $curdir/countmin.[ch] ntop-$UPSTREAM_DIR/
    cp $curdir/prng.[ch] ntop-$UPSTREAM_DIR/

    # remove non-DFSG-compliant part of ntop.h
    ed ntop-$UPSTREAM_DIR/ntop.h > /dev/null <<EOF
/Declaration of POSIX directory browsing functions and types for Win32.
?/\* \** \*/?,/\/\* \** \*\//d
wq
EOF

    # remove old and incorrect license statement from protocols.c
    ed ntop-$UPSTREAM_DIR/protocols.c > /dev/null <<EOF
/The Regents of the University of California.  All rights reserved.
?/\*?,/\*\//d
wq
EOF

    mv ntop-$UPSTREAM_DIR ntop-$DEB_SOURCE_VERSION

    cd ntop-$DEB_SOURCE_VERSION

    rm -fr nDPI
    svn export -r $NDPI_REV https://svn.ntop.org/svn/ntop/trunk/nDPI/
    cd ..

    GZIP=--best tar -cz --owner root --group root --mode a+rX \
        -f ntop_$DEB_SOURCE_VERSION.orig.tar.gz ntop-$DEB_SOURCE_VERSION

    mv ntop_$DEB_SOURCE_VERSION.orig.tar.gz ..

    rm -fr ntop-$DEB_SOURCE_VERSION
fi
