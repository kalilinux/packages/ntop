Description: Remove IP fragment handling code
Author: Ludovico Cavedon <cavedon@debian.org>
Origin: https://svn.ntop.org/svn/ntop/trunk/ntop, commit:5629
Bug-Debian: http://bugs.debian.org/700442

Index: ntop/initialize.c
===================================================================
--- ntop.orig/initialize.c	2012-11-30 00:34:29.909618091 -0800
+++ ntop/initialize.c	2013-02-24 23:10:11.543717767 -0800
@@ -356,8 +356,6 @@
       myGlobals.device[i].sessions = (IPSession**)calloc(sizeof(IPSession*), MAX_TOT_NUM_SESSIONS);
     } else
       myGlobals.device[i].sessions = NULL;    
-
-    myGlobals.device[i].fragmentList = NULL;
   }
 
   myGlobals.hashCollisionsLookup = 0;
Index: ntop/ip.c
===================================================================
--- ntop.orig/ip.c	2013-02-24 23:04:13.296300107 -0800
+++ ntop/ip.c	2013-02-24 23:08:26.902729699 -0800
@@ -30,256 +30,6 @@
 
 /* ***************************************** */
 
-/* *****************************************
- *
- * Fragment handling code courtesy of
- * Andreas Pfaller <apfaller@yahoo.com.au>
- *
- * NOTE:
- * the code below has a small (neglictable) limitation
- * as described below.
- *
- * Subject: ntop 1.3.2: Fragment handling
- * Date:    Mon, 7 Aug 2000 16:05:45 +0200
- * From:    a.pfaller@pop.gun.de (Andreas Pfaller)
- *   To:    l.deri@tecsiel.it (Luca Deri)
- *
- * I have also had a look at the code you added to handle
- * overlapping  fragments. It again assumes specific package
- * ordering (either 1,2,..,n  or  n,n-1,..,1) which the IP protocol
- * does not guarantee. The above assumptions are probably true
- * for most users but in some setups they are nearly never true.
- * Consider two host connected by multiple network cards
- *
- * e.g.:
- *      +--------+ eth0         eth0 +--------+
- *      |        |-------------------|        |
- *      | HOST A |                   | HOST B |
- *      |        |-------------------|        |
- *      +--------+ eth1         eth1 +--------+
- *
- * which distribute traffic on this interfaces to achive better
- * throughput (Called bonding in Linux, Etherchannel by Cisco or
- * trunking by Sun). A simple algorithm simple uses the interfaces
- * in a cyclic way. Since packets are not always the same length
- * or the interfaces my have different speeds more complicated
- * ones use other methods to try to achive maximum throughput.
- * In such an environment you have very high probability for
- * out of order packets.
- *
- * ***************************************** */
-
-#ifdef FRAGMENT_DEBUG
-static void dumpFragmentData(IpFragment *fragment) {
-  printf("FRAGMENT_DEBUG: IPFragment: (%p)\n", fragment);
-  printf("                            %s:%d->%s:%d\n",
-         fragment->src->hostResolvedName, fragment->sport,
-         fragment->dest->hostResolvedName, fragment->dport);
-  printf("                            FragmentId=%d\n", fragment->fragmentId);
-  printf("                            lastOffset=%d, totalPacketLength=%d\n",
-         fragment->lastOffset, fragment->totalPacketLength);
-  printf("                             totalDataLength=%d, expectedDataLength=%d\n",
-         fragment->totalDataLength, fragment->expectedDataLength);
-  fflush(stdout);
-}
-#endif
-
-/* ************************************ */
-
-static IpFragment *searchFragment(HostTraffic *srcHost,
-				  HostTraffic *dstHost,
-				  u_int fragmentId,
-				  int actualDeviceId) {
-  IpFragment *fragment = myGlobals.device[actualDeviceId].fragmentList;
-
-  while ((fragment != NULL)
-         && ((fragment->src != srcHost)
-	     || (fragment->dest != dstHost)
-	     || (fragment->fragmentId != fragmentId)))
-    fragment = fragment->next;
-
-  return(fragment);
-}
-
-/* ************************************ */
-
-void deleteFragment(IpFragment *fragment, int actualDeviceId) {
-
-  if(fragment->prev == NULL)
-    myGlobals.device[actualDeviceId].fragmentList = fragment->next;
-  else
-    fragment->prev->next = fragment->next;
-
-  free(fragment);
-  myGlobals.num_queued_fragments--;
-}
-
-/* ************************************ */
-
-/* Courtesy of Andreas Pfaller <apfaller@yahoo.com.au> */
-static void checkFragmentOverlap(HostTraffic *srcHost,
-                                 HostTraffic *dstHost,
-                                 IpFragment *fragment,
-                                 u_int fragmentOffset,
-                                 u_int dataLength,
-				 int actualDeviceId,
-				 const struct pcap_pkthdr *h, const u_char *p) {
-  if(fragment->fragmentOrder == FLAG_UNKNOWN_FRAGMENT_ORDER) {
-    if(fragment->lastOffset > fragmentOffset)
-      fragment->fragmentOrder = FLAG_DECREASING_FRAGMENT_ORDER;
-    else
-      fragment->fragmentOrder = FLAG_INCREASING_FRAGMENT_ORDER;
-  }
-
-  if((fragment->fragmentOrder == FLAG_INCREASING_FRAGMENT_ORDER
-      && fragment->lastOffset+fragment->lastDataLength > fragmentOffset)
-     ||
-     (fragment->fragmentOrder == FLAG_DECREASING_FRAGMENT_ORDER
-      && fragment->lastOffset < fragmentOffset+dataLength)) {
-    if(myGlobals.runningPref.enableSuspiciousPacketDump) {
-      char buf[LEN_GENERAL_WORK_BUFFER];
-      safe_snprintf(__FILE__, __LINE__, buf, LEN_GENERAL_WORK_BUFFER,
-		    "Detected overlapping packet fragment [%s->%s]: "
-		    "fragment id=%d, actual offset=%d, previous offset=%d\n",
-		    fragment->src->hostResolvedName,
-		    fragment->dest->hostResolvedName,
-		    fragment->fragmentId, fragmentOffset,
-		    fragment->lastOffset);
-
-      dumpSuspiciousPacket(actualDeviceId, h, p);
-    }
-
-    allocateSecurityHostPkts(fragment->src); allocateSecurityHostPkts(fragment->dest);
-    incrementUsageCounter(&fragment->src->secHostPkts->overlappingFragmentSent,
-			  dstHost, actualDeviceId);
-    incrementUsageCounter(&fragment->dest->secHostPkts->overlappingFragmentRcvd,
-			  srcHost, actualDeviceId);
-    incrementTrafficCounter(&myGlobals.device[actualDeviceId].securityPkts.overlappingFragment, 1);
-  }
-}
-
-/* ************************************ */
-
-static u_int handleFragment(HostTraffic *srcHost,
-                            HostTraffic *dstHost,
-                            u_short *sport,
-                            u_short *dport,
-                            u_int fragmentId,
-                            u_int off,
-                            u_int packetLength,
-                            u_int dataLength,
-			    int actualDeviceId,
-			    const struct pcap_pkthdr *h, const u_char *p) {
-  IpFragment *fragment;
-  u_int fragmentOffset, length;
-
-  if(!myGlobals.enableFragmentHandling)
-    return(0);
-
-  accessMutex(&myGlobals.fragmentMutex, "handleFragment");
-
-  fragmentOffset = (off & 0x1FFF)*8;
-
-  fragment = searchFragment(srcHost, dstHost, fragmentId, actualDeviceId);
-
-  if(fragment == NULL) {
-    /* new fragment */
-    fragment = (IpFragment*)calloc(1, sizeof(IpFragment));
-    if(fragment == NULL) return(0); /* out of memory, not much we can do */
-    memset(fragment, 0, sizeof(IpFragment));
-    fragment->src = srcHost, fragment->dest = dstHost;
-    fragment->fragmentId = fragmentId, fragment->firstSeen = myGlobals.actTime;
-    fragment->fragmentOrder = FLAG_UNKNOWN_FRAGMENT_ORDER;
-    fragment->next = myGlobals.device[actualDeviceId].fragmentList, fragment->prev = NULL;
-    if(fragment->next) fragment->next->prev = fragment;
-    myGlobals.device[actualDeviceId].fragmentList = fragment;
-    myGlobals.num_queued_fragments++;
-  } else
-    checkFragmentOverlap(srcHost, dstHost, fragment,
-			 fragmentOffset, dataLength,
-			 actualDeviceId, h, p);
-
-  fragment->lastOffset = fragmentOffset;
-  fragment->totalPacketLength += packetLength;
-  fragment->totalDataLength += dataLength;
-  fragment->lastDataLength = dataLength;
-
-  if(fragmentOffset == 0) {
-    /* first fragment contains port numbers */
-    fragment->sport = *sport;
-    fragment->dport = *dport;
-  } else if(!(off & IP_MF)) /* last fragment->we know the total data size */
-    fragment->expectedDataLength = fragmentOffset+dataLength;
-
-#ifdef FRAGMENT_DEBUG
-  dumpFragmentData(fragment);
-#endif
-
-  /* Now check if we have all the data needed for the statistics */
-  if((fragment->sport != 0) && (fragment->dport != 0) /* first fragment rcvd */
-     /* last fragment rcvd */
-     && (fragment->expectedDataLength != 0)
-     /* probably all fragments rcvd */
-     && (fragment->totalDataLength >= fragment->expectedDataLength)) {
-    *sport = fragment->sport;
-    *dport = fragment->dport;
-    length = fragment->totalPacketLength;
-    deleteFragment(fragment, actualDeviceId);
-  } else {
-    *sport = 0;
-    *dport = 0;
-    length = 0;
-  }
-
-  releaseMutex(&myGlobals.fragmentMutex);
-
-  return length;
-}
-
-/* ************************************ */
-
-void purgeOldFragmentEntries(int actualDeviceId) {
-  IpFragment *fragment, *next;
-#ifdef FRAGMENT_DEBUG 
-  u_int fragcnt=0, expcnt=0;
-#endif
-
-  accessMutex(&myGlobals.fragmentMutex, "purgeOldFragmentEntries");
-
-  fragment = myGlobals.device[actualDeviceId].fragmentList;
-
-  while(fragment != NULL) {
-#ifdef FRAGMENT_DEBUG
-    fragcnt++;
-#endif
-    next = fragment->next;
-    if((fragment->firstSeen + 30 /* sec */) < myGlobals.actTime) {
-#ifdef FRAGMENT_DEBUG
-      expcnt++;
-      dumpFragmentData(fragment);
-#endif
-
-      if(fragment->prev) fragment->prev = next;
-      if(next)           next->prev = fragment->prev;
-
-      deleteFragment(fragment, actualDeviceId);
-    }
-
-    fragment = next;
-  }
-
-  releaseMutex(&myGlobals.fragmentMutex);
-
-#ifdef FRAGMENT_DEBUG
-  if(fragcnt) {
-    printf("FRAGMENT_DEBUG: fragcnt=%d, expcnt=%d\n", fragcnt, expcnt);
-    fflush(stdout);
-  }
-#endif
-}
-
-/* ************************************ */
-
 /*
   Fingerprint code courtesy of ettercap
   http://ettercap.sourceforge.net
@@ -728,25 +478,6 @@
       sport = ntohs(tp.th_sport);
       dport = ntohs(tp.th_dport);
 
-      /*
-	Don't move this code on top as it is supposed to stay here
-	as it modifies sport/sport
-
-	Courtesy of Andreas Pfaller
-      */
-      if(myGlobals.enableFragmentHandling && (fragmented)) {
-	/* Handle fragmented packets */
-	if(ip6)
-	  length = handleFragment(srcHost, dstHost, &sport, &dport,
-				  (u_short)(ip6->ip6_flow & 0xffff),fragmented,
-				  length,ntohs(ip6->ip6_plen),
-				  actualDeviceId, h, p);
-	else
-	  length = handleFragment(srcHost, dstHost, &sport, &dport,
-				  ntohs(ip.ip_id), off, length,
-				  ip_len - hlen, actualDeviceId, h, p);
-      }
-
       if(srcHost->fingerprint == NULL) {
 	char fingerprint[64] = { 0 } ;
 	int WIN=0, MSS=-1, WS=-1, S=0, N=0, D=0, T=0;
@@ -1056,24 +787,6 @@
 	}
       }
 
-      /*
-	Don't move this code on top as it is supposed to stay here
-	as it modifies sport/sport
-
-	Courtesy of Andreas Pfaller
-      */
-      if(myGlobals.enableFragmentHandling && (fragmented)) {
-	/* Handle fragmented packets */
-	if(ip6)
-	  length = handleFragment(srcHost, dstHost, &sport, &dport,
-				  (u_short)(ip6->ip6_flow & 0xffff), fragmented, length,
-				  ntohs(ip6->ip6_plen), actualDeviceId, h, p);
-	else
-	  length = handleFragment(srcHost, dstHost, &sport, &dport,
-				  ntohs(ip.ip_id), off, length,
-				  ip_len - hlen, actualDeviceId, h, p);
-      }
-
       if((sport > 0) || (dport > 0)) {
 	updateInterfacePorts(actualDeviceId, sport, dport, length);
 	updateUsedPorts(srcHost, dstHost, sport, dport, udpDataLength);
Index: ntop/webInterface.c
===================================================================
--- ntop.orig/webInterface.c	2013-02-24 23:11:15.048316261 -0800
+++ ntop/webInterface.c	2013-02-24 23:11:33.276487868 -0800
@@ -6391,8 +6391,6 @@
 
   printFeatureConfigInfo(textPrintFlag, "Protocol Decoders",
 			 pref->enablePacketDecoding == 1 ? "Enabled" : "Disabled");
-  printFeatureConfigInfo(textPrintFlag, "Fragment Handling",
-			 myGlobals.enableFragmentHandling == 1 ? "Enabled" : "Disabled");
   printFeatureConfigInfo(textPrintFlag, "Tracking only local hosts",
 			 pref->trackOnlyLocalHosts == 1 ? "Yes" : "No");
   safe_snprintf(__FILE__, __LINE__, buf, sizeof(buf), "%d",
@@ -6911,10 +6909,6 @@
 
   /* **** */
 
-  printInfoSectionTitle(textPrintFlag, "Fragments Handling");
-  safe_snprintf(__FILE__, __LINE__, buf, sizeof(buf), "%u", myGlobals.num_queued_fragments);
-  printFeatureConfigInfo(textPrintFlag, "Queued Fragments", buf);
-
   printInfoSectionTitle(textPrintFlag, "----- Address Resolution -----");
 
   printInfoSectionTitle(textPrintFlag, "DNS Sniffing (other hosts requests)");
Index: ntop/globals-core.c
===================================================================
--- ntop.orig/globals-core.c	2013-02-18 04:53:17.716460658 -0800
+++ ntop/globals-core.c	2013-02-24 23:16:56.751522248 -0800
@@ -166,9 +166,6 @@
   myGlobals.checkVersionStatus = FLAG_CHECKVERSION_NOTCHECKED;
   myGlobals.checkVersionStatusAgain = 1;
 
-  /* Other flags (to be set via command line options one day) */
-  myGlobals.enableFragmentHandling = 1;
-
   /* Search paths */
   myGlobals.dataFileDirs    = _dataFileDirs;
   myGlobals.pluginDirs      = _pluginDirs;
@@ -214,7 +211,6 @@
   myGlobals.runningPref.sslPort = 0; /* Disabled by default: enabled via -W */
 #endif
 
-  myGlobals.num_queued_fragments = 0;
   myGlobals.dnsSniffCount = 0;
   myGlobals.dnsSniffRequestCount = 0;
   myGlobals.dnsSniffFailedCount = 0;
@@ -297,7 +293,6 @@
   createMutex(&myGlobals.purgeMutex);       /* synchronize purging */
   createMutex(&myGlobals.securityItemsMutex);
   createMutex(&myGlobals.hostsHashLockMutex);
-  createMutex(&myGlobals.fragmentMutex);
 
   createMutex(&myGlobals.serialLockMutex);  /* Serial host locking */
 
Index: ntop/globals-core.h
===================================================================
--- ntop.orig/globals-core.h	2013-02-18 04:53:17.716460658 -0800
+++ ntop/globals-core.h	2013-02-24 23:17:23.975776667 -0800
@@ -399,8 +399,6 @@
 			 u_char *ether_dst,
 			 int actualDeviceId,
 			 int vlanId);
-extern void deleteFragment(IpFragment *fragment, int actualDeviceId);
-extern void purgeOldFragmentEntries(int actualDeviceId);
 
 /* pbuf.c */
 extern void allocateSecurityHostPkts(HostTraffic *srcHost);
Index: ntop/globals-structtypes.h
===================================================================
--- ntop.orig/globals-structtypes.h	2013-02-18 04:53:17.720460687 -0800
+++ ntop/globals-structtypes.h	2013-02-24 23:16:21.791195306 -0800
@@ -1438,7 +1438,6 @@
 
   /* ************************** */
 
-  IpFragment *fragmentList;
   IPSession **sessions;
   u_int numSessions, maxNumSessions;
 
@@ -1910,8 +1909,6 @@
   char *dbPath;                 /* 'P' */
   char *spoolPath;              /* 'Q' */
   struct fileList *pcap_file_list; /* --pcap-file-list */
-  /* Other flags (these could set via command line options one day) */
-  bool enableFragmentHandling;
 
   HostsDisplayPolicy hostsDisplayPolicy;
   LocalityDisplayPolicy localityDisplayPolicy;
@@ -1959,10 +1956,6 @@
    */
   PthreadMutex purgeMutex;
 
-  /* Fragments */
-  u_int num_queued_fragments;
-  PthreadMutex fragmentMutex;
-
   /*
    * HTS - Host Traffic Statistics
    */
Index: ntop/hash.c
===================================================================
--- ntop.orig/hash.c	2012-11-30 00:34:29.869618288 -0800
+++ ntop/hash.c	2013-02-24 23:18:14.024244031 -0800
@@ -398,8 +398,6 @@
   myGlobals.piMem = (u_int)(maxHosts*sizeof(HostTraffic*));
   theFlaggedHosts = (HostTraffic**)calloc(1, myGlobals.piMem);
 
-  purgeOldFragmentEntries(actDevice); /* let's do this too */
-
 #ifdef IDLE_PURGE_DEBUG
   traceEvent(CONST_TRACE_INFO, "IDLE_PURGE_DEBUG: accessMutex(purgeMutex)...calling");
 #endif
Index: ntop/ntop.c
===================================================================
--- ntop.orig/ntop.c	2012-11-30 00:34:29.941617926 -0800
+++ ntop/ntop.c	2013-02-24 23:17:40.495931031 -0800
@@ -1030,12 +1030,6 @@
   for(i=0; i<myGlobals.numDevices; i++) {
     freeHostInstances(i);
     freeDeviceSessions(i);
-
-    while(myGlobals.device[i].fragmentList != NULL) {
-      IpFragment *fragment = myGlobals.device[i].fragmentList->next;
-      free(myGlobals.device[i].fragmentList);
-      myGlobals.device[i].fragmentList = fragment;
-    }
   }
 
   freeHostInfo(myGlobals.broadcastEntry, 0); myGlobals.broadcastEntry = NULL;
Index: ntop/term.c
===================================================================
--- ntop.orig/term.c	2012-11-30 00:34:29.961617827 -0800
+++ ntop/term.c	2013-02-24 23:18:06.148170547 -0800
@@ -76,9 +76,6 @@
     }
 
     myGlobals.device[j].numSessions = 0;
-    
-    while(myGlobals.device[j].fragmentList != NULL)
-      deleteFragment(myGlobals.device[j].fragmentList, j);
   }
 }
 
